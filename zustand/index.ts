import create from "zustand";
import { devtools, persist } from "zustand/middleware";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MergedProblem } from "../API/Codeforces/API/advenced";

export type BearState = {
  bears: number;
  increase: (by: number) => void;
  problem?: MergedProblem;
  problemTutorialUrl?: string;
  setProblem: (problem: MergedProblem) => void;
  setProblemTutorialUrl: (tutorialUrl: string) => void;
};

export const useBearStore = create<BearState>()(
  devtools(
    persist(
      (set) => ({
        bears: 0,
        increase: (by) => set((state) => ({ bears: state.bears + by })),

        problem: undefined,
        setProblem: (problem) =>
          set((state) => ({
            problem,
            problemTutorialUrl:
              state.problem !== problem ? undefined : state.problemTutorialUrl,
          })),
        problemTutorialUrl: undefined,
        setProblemTutorialUrl: (tutorialUrl) =>
          set((state) => ({
            problemTutorialUrl: tutorialUrl,
          })),
      }),
      {
        name: "problem-storage",
        getStorage: () => AsyncStorage,
      }
    )
  )
);
