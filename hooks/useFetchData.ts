import { useEffect, useState } from "react";

export type PageData = any;

export const fetchData = (url: string) => {
  const [data, setData] = useState<PageData>();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<any>();

  useEffect(() => {
    const controller = new AbortController();
    setLoading((_) => true);
    const fetchData = async () => {
      // await new Promise(r => setTimeout(r, 500));

      const promise = fetch(url, {
        method: "get",
        signal: controller.signal,
      });
      const data = await promise;
      return await data.json();
    };

    fetchData()
      .then((result) => {
        setData(result);
        setLoading((_) => false);
        console.log(result);
      })
      .catch((error) => {
        setError(error);
      });

    return () => {
      setLoading((_) => false);
      controller.abort();
    };
  }, [url]);
  return { data, loading, error };
};
