import { useMemo, useState } from "react";

class Pagination<T> {
  readonly elems: T[];
  /** 1-based index */
  page: number;
  /** number of elements per page */
  readonly pageCount: number;

  constructor(elems: T[], pageCount: number, initPage: number = 1) {
    this.elems = elems;
    this.pageCount = pageCount;
    this.page = initPage;
  }

  private getPageOffset(page: number) {
    return (page - 1) * this.pageCount;
  }

  lastPage() {
    return Math.ceil(this.elems.length / this.pageCount);
  }

  getPageElemsNumber(n?: number) {
    if (this.elems.length === 0) return 0;
    if (!n) n = this.page;
    if (n !== this.lastPage()) return this.pageCount;
    const r = this.elems.length % this.pageCount;
    if (r === 0) return this.pageCount;
    return r;
  }

  getPageElems(n?: number) {
    const pageNumber = n ?? this.page;
    const pageOffset = this.getPageOffset(pageNumber);
    return [...Array(this.getPageElemsNumber(pageNumber)).keys()].map(
      (e) => this.elems[e + pageOffset]
    );
  }

  nextPage() {
    if (this.page !== this.lastPage()) this.page += 1;
    return this.page;
  }

  prevPage() {
    if (this.page !== 1) this.page -= 1;
    return this.page;
  }
}

export const usePagination = <T>(
  elems: T[],
  pageCount: number,
  initPage: number = 1
) => {
  const [page, setPage] = useState(initPage);
  const content = useMemo(
    () => new Pagination(elems, pageCount, initPage),
    [elems, pageCount, initPage]
  );
  const result = useMemo(
    () => ({
      page,
      elems: content.getPageElems(page),
      prevPage: () => setPage((p) => content.prevPage()),
      nextPage: () => setPage((p) => content.nextPage()),
    }),
    [content, page]
  );

  return result;
};
