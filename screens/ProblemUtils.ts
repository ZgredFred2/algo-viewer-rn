import { MergedProblem } from "../API/Codeforces/API/advenced";

export const selectUri = (id: number) => {
  switch (id) {
    case 1:
      return "https://codeforces.com/problemset/problem/1749/B";
    case 2:
      return "https://codeforces.com/problemset/problem/1754/B";
    case 3:
      return "https://codeforces.com/problemset/problem/1747/D";
    case 4:
      return "https://codeforces.com/problemset/problem/1753/D";
  }
};

const prefix = "https://codeforces.com/problemset/problem";

export const problemDataToUrl = (
  problem: MergedProblem,
  mobile: boolean = true
) => problemToUrl(problem.contestId, problem.index, mobile);

export const problemToUrl = (
  contestId: number,
  index: string,
  mobileVersion: boolean = true
) => `${prefix}/${contestId}/${index.toUpperCase()}?mobile=${mobileVersion}`;

const codeforcesStyle = `
    <style>
      .problem-statement{
        font-size: 1.8em;
      }
      .problem-statement .sample-tests .sample-test{
        font-size:1.4em;
      }
      #body {
        width: 100vw!important;
        max-width: 100vw;
        min-width: 300px;
      }
    </style> `;

const bodyStyle = `width: 100% !important; max-width: 100% !important; min-width: 300px !important;`;

const codeforcesJs = `
    document.querySelector("#header")?.remove();
    document.querySelector("#footer")?.remove();
    document.querySelector(".second-level-menu")?.remove();
    document.querySelector(".mobile-toolbar")?.remove();
    document.querySelector(".alert")?.remove();

    function updateStyle(){
      // document.querySelector(".problem-statement").style.fontSize="1.8em";
      document.querySelector(".problem-statement").style.fontSize="62px";
      // document.querySelector(".problem-statement .sample-tests .sample-test").style.fontSize="1.4em";
      document.querySelector(".problem-statement .sample-tests .sample-test").style.fontSize="108px";
      const b = document.querySelector("#body");

      b.setAttribute("style", '${bodyStyle}');
    }
    function getElem(){
      const tutorialElement = document.querySelector("a[title*='Editorial']");
      window.ReactNativeWebView.postMessage(tutorialElement.href);
    }
    // TODO: make sure MathJax rendered then update styles
    const id = setTimeout(updateStyle,1000);
    const id2 = setTimeout(getElem, 500);
  `;

export const appendInlineStyle = (pageText: string, style: string) => {
  const findText = "</head>";
  const index = pageText.indexOf(findText);
  const prefix = pageText.slice(0, index);
  const rest = pageText.slice(index);
  return prefix + style + rest;
};

export const loadPageText = async (url: string) => {
  const e = await fetch(url);
  return await e.text();
};

export const getStylesAndJavaScript = (url: string) => {
  if (url.startsWith("https://codeforces.com")) {
    return { js: codeforcesJs, styles: codeforcesStyle };
  }
  return { js: "", styles: "" };
};
