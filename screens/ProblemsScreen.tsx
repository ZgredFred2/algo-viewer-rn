import { useEffect, useState } from "react";
import { ActivityIndicator, View } from "react-native";
import WebView from "react-native-webview";
import { MergedProblem } from "../API/Codeforces/API/advenced";
import { Text } from "../components/Themed";
import useColorScheme from "../hooks/useColorScheme";
import { RootTabScreenProps } from "../types";
import { BearState, useBearStore } from "../zustand";
import {
  appendInlineStyle,
  getStylesAndJavaScript,
  loadPageText,
  problemToUrl,
} from "./ProblemUtils";
import { useWindowDimensions } from "react-native";

export type ProblemScreenProps = RootTabScreenProps<"ProblemsTab">;

export type ProblemScreenProps2 = RootTabScreenProps<"ProblemsTab"> &
  BearState & { problem: MergedProblem };

export function LoadingIndicatorView() {
  return <ActivityIndicator color="#009b88" size="large" />;
}

export const ProblemsScreen = (props: ProblemScreenProps) => {
  const store = useBearStore((store) => store);

  if (!store.problem) return <InfoScreen />;
  return <ProblemsScreen2 {...props} {...store} problem={store.problem} />;
};

export const ProblemsScreen2 = (props: ProblemScreenProps2) => {
  const colorScheme = useColorScheme();
  const setTutorialUrl = useBearStore((store) => store.setProblemTutorialUrl);
  const { width, height } = useWindowDimensions();

  const problemUrl = problemToUrl(props.problem.contestId, props.problem.index);

  const [pageText, setPageText] = useState("");
  const [jsCode, setJsCode] = useState("");

  // fetch pageHtmlOnChange
  useEffect(() => {
    const buildPage = async () => {
      const pageText = await loadPageText(problemUrl);
      const mods = getStylesAndJavaScript(problemUrl);
      const modifiedPage = appendInlineStyle(pageText, "" ?? mods.styles);
      setPageText(modifiedPage);
      setJsCode(mods.js);
    };
    buildPage();
    console.log(problemUrl);
  }, [problemUrl, getStylesAndJavaScript]);

  return (
    <View style={{ flex: 1 }}>
      {/* opening in portrait mode than changing to landscape result in ~70% with use */}
      {/* sometimes doing little zoom in */}
      <WebView
        source={{
          uri: problemUrl,
        }}
        renderLoading={LoadingIndicatorView}
        startInLoadingState={true}
        style={{
          flex: 1,
        }}
        forceDarkOn={colorScheme === "dark" ? true : undefined}
        injectedJavaScript={jsCode}
        onMessage={(e) => {
          const data = e.nativeEvent.data;
          console.log("received:", data);
          if (typeof data === "string") {
            setTutorialUrl(data);
          }
        }}
      />
    </View>
  );
};

type InfoScreenProps = any;
const InfoScreen = (props: InfoScreenProps) => {
  return (
    <View>
      <Text>select problem in ploblem list</Text>
    </View>
  );
};
