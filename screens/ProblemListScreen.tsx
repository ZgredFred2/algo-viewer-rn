import { FontAwesome } from "@expo/vector-icons";
import {
  Box,
  Button,
  Center,
  Heading,
  HStack,
  Icon,
  Pressable,
  ScrollView,
  Text,
  VStack,
} from "native-base";
import { useEffect, useState } from "react";
import {
  getProblems,
  MergedData,
  MergedProblem,
} from "../API/Codeforces/API/advenced";
import { colorContests, ColorMapper } from "../API/Codeforces/Colors";
import { View } from "../components/Themed";
import { usePagination } from "../hooks/usePagination";

import { RootTabScreenProps } from "../types";
import { useBearStore } from "../zustand";

export default function ProblemListScreen(
  props: RootTabScreenProps<"Problem List">
) {
  const [mergedData, setMergedData] = useState<MergedData>({});
  const [problems, setProblems] = useState<MergedProblem[]>([]);
  const [loadingStage, setLoadingStage] = useState<
    "undefined" | "loading" | "loaded"
  >("undefined");
  const [contests, setContests] = useState<MergedProblem[][]>([]);
  const [colorizer, setcolorizer] = useState<ColorMapper>({});
  const store = useBearStore((store) => store);

  // store contests instead of problems
  const content = usePagination(contests, 1);

  useEffect(() => {
    const fetchData = async () => {
      if (loadingStage !== "undefined") return;
      setLoadingStage("loading");
      const data = await getProblems("tourist");
      setMergedData(data.mergedData);
      setProblems(data.problems);
      const contests = packByContestId(data.problems);
      setContests((_) => contests);
      setLoadingStage("loaded");
    };
    fetchData().catch((e) => console.log(e));
  }, []);

  useEffect(() => {
    const colorizer = colorContests(problems);
    setcolorizer(colorizer);
  }, [colorContests, problems]);

  const packByContestId = <T extends { contestId: number }>(elems: T[]) =>
    elems.reduce((prev, curr) => {
      if (prev.length === 0) return [[curr]];
      const prevVal = prev[prev.length - 1];
      if (prevVal[0].contestId === curr.contestId) prevVal.push(curr);
      else prev.push([curr]);
      return prev;
    }, [] as T[][]);

  // @render
  return (
    <ScrollView>
      <VStack space={4}>
        <HStack>
          <Button
            onPress={() => {
              content.prevPage();
            }}
          >
            go prev
          </Button>
          <Button
            onPress={() => {
              content.nextPage();
            }}
          >
            go next
          </Button>
        </HStack>
        {content.elems.map((contestBox) => (
          <VStack
            key={contestBox[0].contestId}
            borderWidth={1}
            borderColor={"cyan.50"}
            padding={3}
            space={1}
          >
            <Center>
              <Heading>{contestBox[0].contestId}</Heading>
            </Center>
            {contestBox.map((e, idx, arr) => {
              const solved = e.submissions?.some((e) => e.verdict === "OK");
              return (
                <HStack
                  flex={1}
                  key={e.code}
                  bgColor={colorizer[e.contestId]
                    ?.desaturate((Math.min(e.indexNumber, 5) - 1) * 0.2)
                    .hex()}
                  borderBottomRadius={idx === arr.length - 1 ? 5 : 0}
                  borderTopRadius={idx === 0 ? 5 : 0}
                  paddingX={2}
                >
                  <Pressable
                    flex={1}
                    onPress={() => {
                      store.setProblem(e);
                      props.navigation.navigate("ProblemsTab");
                    }}
                  >
                    <HStack flex={1} justifyContent={"space-between"}>
                      <HStack flexBasis={"80%"}>
                        <Center>
                          <Text width={10} fontSize={25}>
                            {e.index}
                          </Text>
                        </Center>
                        <Center>
                          <Text fontSize={20}>{e.name}</Text>
                        </Center>
                      </HStack>

                      <Center>
                        <HStack>
                          {solved && (
                            <View>
                              <FontAwesome
                                color="green"
                                name="check-circle"
                                size={18}
                              />
                            </View>
                          )}
                          <Text width={50}>{e.solvedCount}</Text>
                        </HStack>
                      </Center>
                    </HStack>
                  </Pressable>
                </HStack>
              );
            })}
          </VStack>
        ))}
      </VStack>
    </ScrollView>
  );
}
