import { Center, Text, View } from "native-base";
import WebView from "react-native-webview";
import useColorScheme from "../hooks/useColorScheme";

import { RootTabScreenProps } from "../types";
import { useBearStore } from "../zustand";
import { LoadingIndicatorView } from "./ProblemsScreen";

const jsGen = () => (problem: { contestId: number; index: string }) => {
  // @ts-ignore
  problem = JSON.parse(problem);

  const clg = (s: string | undefined | null) => {
    // @ts-ignore
    window.ReactNativeWebView!.postMessage(s);
  };
  const removeDecorators = () => {
    const header = document.querySelector("#header");
    if (header) header.remove();
    const footer = document.querySelector("#footer");
    if (footer) footer.remove();
    const menu = document.querySelector(".second-level-menu");
    if (menu) menu.remove();
    const toolbar = document.querySelector(".mobile-toolbar");
    if (toolbar) toolbar.remove();
    const alertMessage = document.querySelector(".alert");
    if (alertMessage) alertMessage.remove();
  };
  const updateStyles = () => {
    const bodyStyle = `
    width: 100% !important;
    max-width: 100% !important;
    min-width: 300px !important;
    overflow-x: scroll;
    `;
    const b = document.querySelector<HTMLElement>("#body");
    if (!b) clg("no #body found");
    b?.setAttribute("style", `${bodyStyle}`);
    const problemStatment =
      document.querySelectorAll<HTMLElement>(".problem-statement");
    Array.from(problemStatment).forEach((e) => {
      e.style.fontSize = "65px";
    });
    // if (b) b.style.fontSize = "64px";
  };

  const modifyDOM = () => {
    clg("-----------------------------");
    const selectedSpecific = document.querySelectorAll<HTMLElement>(
      `h3:has(>a[href*="/contest/${problem.contestId}/problem/${problem.index}"])`
    );
    const topicElem = document.querySelector(".topic");
    clg("tipic tag:" + topicElem?.tagName);
    const aElem = selectedSpecific[0];
    clg("specific elems:" + selectedSpecific.length);
    const elems = document.querySelectorAll<HTMLElement>(
      `a[href*="/contest/${problem.contestId}/problem"]`
    );

    const mainNode = aElem.parentElement;
    if (mainNode) topicElem?.appendChild(mainNode);
    clg(mainNode?.tagName);

    const pageContentElement = document.querySelector("#body");
    const contentElems = Array.from(pageContentElement?.children ?? []);
    contentElems.forEach((e) => {
      e.remove();
    });
    if (mainNode) pageContentElement?.appendChild(mainNode);

    clg("aElem:" + aElem?.innerHTML);
    // if (aElem) aElem.style.backgroundColor = "red";
    if (aElem) aElem.remove();
  };
  removeDecorators();
  const id = setTimeout(updateStyles, 1500);
  const id2 = setTimeout(modifyDOM, 1000);
};

const jsMod = (jsFun: string) => {
  let js = jsFun.replace("function", "function f");
  js = js.replace(/var/g, "let");
  // console.log(js);
  return js;
};

export default function TutorialScreen(props: RootTabScreenProps<"Tutorial">) {
  const store = useBearStore((store) => store);
  const colorScheme = useColorScheme();
  const argCall = JSON.stringify({
    contestId: store.problem?.contestId,
    index: store.problem?.index,
  });
  const func = jsGen() + `f('${argCall}');`;
  const js = jsMod(func.toString());

  return (
    <View style={{ flex: 1 }}>
      {store.problemTutorialUrl && store.problem && (
        <WebViewTutorial
          url={store.problemTutorialUrl}
          colorScheme={colorScheme}
          jsCode={js}
        />
      )}
    </View>
  );
}
export type WebViewTutorialProps = {
  url: string;
  colorScheme: string;
  jsCode?: string;
};

export const WebViewTutorial = (props: WebViewTutorialProps) => {
  // let r = useRef<typeof WebView>();
  return (
    <WebView
      // ref={(ref) => (r.current = ref)}
      key={props.jsCode}
      source={{
        uri: props.url,
      }}
      renderLoading={LoadingIndicatorView}
      startInLoadingState={true}
      style={{
        flex: 1,
      }}
      forceDarkOn={props.colorScheme === "dark" ? true : undefined}
      injectedJavaScript={props.jsCode}
      onMessage={(e) => {
        const data = e.nativeEvent.data;
        console.log(data);
      }}
    />
  );
};
