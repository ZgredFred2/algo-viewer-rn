import chroma from "chroma-js";
import { getContestIdList } from "./API/advenced";

export type ColorMapper = { [key: number]: chroma.Color };

const defaultColorizer = (n: number) => {
  return chroma.hsl((n * 140) % 360, 1, 0.28);
};

/**  */
export const colorContests = (
  problemList: { contestId: number }[],
  colorizer: (n: number) => chroma.Color = defaultColorizer
) => {
  const contestsIds = getContestIdList([...problemList].reverse());
  const result = contestsIds.reduce((prev, curr, idx) => {
    prev[curr] = colorizer(idx);
    return prev;
  }, {} as ColorMapper);
  return result;
};
