import assert from "assert";
import { fetchData } from ".";
import { Problem, ProblemStatistic, Submission } from "./types";

const isAlpha = (str: string) => {
  return /^[a-zA-Z]$/.test(str);
};

const isDigit = (str: string) => {
  return /^[0-9]$/.test(str);
};

assert(isAlpha("a") && isAlpha("A"), "should be alpha");
assert(
  !isAlpha("0") && !isAlpha("") && !isAlpha("a0") && !isAlpha("("),
  "should not be alpha"
);

export const genCode = (element: { index: string; contestId?: number }) => {
  const separator = element.index.split("").every((e) => isDigit(e)) ? "-" : "";
  return (element.contestId || "") + separator + (element.index || "");
};

/** gather Problems, ProblemsStatistic and Submissions for user at once */
export const getProblems = async (forUser: string) => {
  const problems = fetchData("problemset.problems", {});
  const userSubmissions = fetchData("user.status", { handle: forUser });
  const problemList =
    (await problems)?.problems.map((e) => ({ ...e, code: genCode(e) })) ?? [];
  const problemStatiscicList =
    (await problems)?.problemStatistics.map((e) => ({
      ...e,
      code: genCode(e),
    })) ?? [];
  const submissionList =
    (await userSubmissions)?.map((e) => ({
      ...e,
      problem: { ...e.problem, code: genCode(e.problem) },
    })) ?? [];

  const mergedData = mergeData(
    problemList,
    problemStatiscicList,
    submissionList
  );
  const array = Object.values(mergedData).sort((a, b) => {
    if (a.contestId === b.contestId) {
      return a.index.localeCompare(b.index);
    }
    if (!a.contestId || !b.contestId) return 0;
    return b.contestId - a.contestId;
  });
  array.forEach((e, idx, arr) => {
    const prev = arr[idx - 1] as MergedProblem | undefined;
    // if we do in correct order, prev will have indexNumber
    const prevIndexNumberFromSameContest =
      prev?.contestId === e.contestId ? prev.indexNumber : 0;
    const myNum = prevIndexNumberFromSameContest + 1;
    arr[idx].indexNumber = myNum;
    mergedData[e.code].indexNumber = myNum;
  });
  return {
    problems: array,
    submissionList,
    mergedData,
  };
};

export type MergedProblem = Problem &
  ProblemStatistic & {
    /** newest first */
    submissions?: Submission[];
    /** contest problem number 1-based */
    indexNumber: number;
  };

export type MergedData = {
  [key: string]: MergedProblem;
};

export const mergeData = (
  problems: Problem[],
  problemsStat: ProblemStatistic[],
  submissions: Submission[]
) => {
  // { "1750A": Problem & problemStat & submission[]}
  // unload problems => unload problemsStat => unload submissions
  // unloading submission consist of appending submisison to its own problem.code
  const mode = "fors" as "fors" | "reduces";
  if (mode === "fors") {
    // console.time("phase1 for");
    const x = {} as { [key: string]: Problem | undefined };
    problems.forEach((e) => {
      if (!e.code) return;
      x[e.code] = e;
    });
    // console.timeEnd("phase1 for"); // 3ms

    // console.time("phase2 for");
    const y = x as {
      [key: string]: Problem | (Problem & ProblemStatistic) | undefined;
    };
    problemsStat.forEach((e) => {
      if (!e.code) return;
      const subject = y[e.code];
      if (!subject) return;
      y[e.code] = { ...subject, ...e };
    });
    // console.timeEnd("phase2 for"); // 8ms

    // console.time("phase3 for");
    const z = y as {
      [key: string]: MergedProblem | undefined;
    };
    submissions.forEach((e) => {
      const code = e.problem.code;
      if (!code) return;
      const obj = z[code];
      if (!obj) return;
      obj.submissions = obj.submissions ?? [];
      obj.submissions.push(e);
    });
    // console.timeEnd("phase3 for"); // 0.4ms

    return z as MergedData;
  }
  //      CODE BELOW HAS TERIBLE EFFICIENCY
  console.time("phase1 reduce");
  const unloadedProblems = problems.reduce((prev, curr) => {
    if (!curr.code) return prev;
    prev[curr.code] = { ...prev[curr.code], ...curr };
    return prev;
    // time 10ms
    // return { ...prev, [curr.code]: { ...prev[curr.code], ...curr } };
    // time: 8800ms
  }, {} as { [key: string]: Problem | undefined });
  console.timeEnd("phase1 reduce");
  console.time("phase2 reduce");
  const unloadedProblemsWithStats = problemsStat.reduce((prev, curr) => {
    prev[curr.code] = { ...prev[curr.code], ...curr };
    return prev;
    // time 24ms
    // return {
    //   ...prev,
    //   [curr.code]: { ...prev[curr.code], ...curr },
    // };
    // time: 19'000ms
  }, unloadedProblems as { [key: string]: Problem & ProblemStatistic });
  console.timeEnd("phase2 reduce");
  console.time("phase3 reduce");
  const withUserSubmissions = submissions.reduce(
    (prev, curr) => {
      const subjectCode = curr.problem.code;
      if (!subjectCode) return prev;
      const newList = [...(prev[subjectCode]?.submissions ?? []), curr];
      let subjectElement = prev[subjectCode];
      if (!subjectElement) return prev;
      subjectElement = { ...subjectElement, submissions: newList };
      prev[subjectCode] = subjectElement;
      return prev;
      // time 0.7ms
      // return {
      //   ...prev,
      //   [subjectCode]: {
      //     ...prev[subjectCode],
      //     submissions: newList,
      //   },
      // };
      // time 740ms
    },
    unloadedProblemsWithStats as {
      [key: string]:
        | (Problem & ProblemStatistic & { submissions?: Submission[] })
        | undefined;
    }
  );
  console.timeEnd("phase3 reduce");
  return withUserSubmissions as MergedData;
};

// probably better to make it generic
/** returns array of contestId's that occure in problems
 * @param problems - array of problems, needs to be in sorted order by `contestId`
 * @example getContestIdList([{contestId: 1,...}, {contestId: 1, ...} , {contestId: 100, ...}]) equals [1, 100]
 * */
export const getContestIdList = (problems: { contestId: number }[]) => {
  return problems.reduce((prev, curr) => {
    if (!curr.contestId) return prev;
    if (prev.length === 0) return [curr.contestId];
    if (prev[prev.length - 1] !== curr.contestId) prev.push(curr.contestId);
    return prev;
  }, [] as number[]);
};
