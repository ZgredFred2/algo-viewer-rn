export type Problem = {
  // should be optional
  /** Id of the contest, containing the problem. */
  contestId: number;
  /** Short name of the problemset the problem belongs to. */
  problemsetName?: string;
  /** Usually, a letter or letter with digit(s) indicating the problem index in a contest. */
  index: string;
  /** Localized. */
  name: string;
  // no description
  type: "PROGRAMMING" | "QUESTION";
  /** Maximum amount of points for the problem. */
  points?: number;
  /** Problem rating (difficulty). */
  rating?: number;
  /** Problem tags. */
  tags: string[];
  // should be optional as well as contestId
  /** contestId and index conbined */
  code: string;
};

export type ProblemStatistic = {
  /** Id of the contest, containing the problem. */
  contestId: number;
  /** Usually, a letter or letter with digit(s) indicating the problem index in a contest. */
  index: string;
  /** Number of users, who solved the problem. */
  solvedCount: number;
  /** contestId and index combined */
  code: string;
};

export type ProblemsetProblemsResult = {
  problems: Problem[];
  problemStatistics: ProblemStatistic[];
};

export type Party = any;

export type Verdict =
  | "FAILED"
  | "OK"
  | "PARTIAL"
  | "COMPILATION_ERROR"
  | "RUNTIME_ERROR"
  | "WRONG_ANSWER"
  | "PRESENTATION_ERROR"
  | "TIME_LIMIT_EXCEEDED"
  | "MEMORY_LIMIT_EXCEEDED"
  | "IDLENESS_LIMIT_EXCEEDED"
  | "SECURITY_VIOLATED"
  | "CRASHED"
  | "INPUT_PREPARATION_CRASHED"
  | "CHALLENGED"
  | "SKIPPED"
  | "TESTING"
  | "REJECTED";

export type Testset =
  | "SAMPLES"
  | "PRETESTS"
  | "TESTS"
  | "CHALLENGES"
  | "TESTS1"
  | "TESTS2"
  | "TESTS3"
  | "TESTS4"
  | "TESTS5"
  | "TESTS6"
  | "TESTS7"
  | "TESTS8"
  | "TESTS9"
  | "TESTS10";

export type Submission = {
  id: number;
  contestId?: number;
  /** Time, when submission was created, in unix-format. */
  creationTimeSeconds: number;
  /** Number of seconds, passed after the start of the contest (or a virtual start for virtual parties), before the submission. */
  relativeTimeSeconds: number;
  problem: Problem;
  author?: Party;
  programmingLanguage: string;
  verdict?: Verdict;
  /** Testset used for judging the submission. */
  testset: Testset;
  /** Number of passed tests. */
  passedTestCount: number;
  /** Maximum time in milliseconds, consumed by solution for one test. */
  timeConsumedMillis: number;
  /** Maximum memory in bytes, consumed by solution for one test. */
  memoryConsumedBytes: number;
  /** Number of scored points for IOI - like contests. Floating point number. */
  points?: number;
};
