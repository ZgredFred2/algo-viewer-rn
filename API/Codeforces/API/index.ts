// also provide paramList
// export type Method =
//   | "blogEntry.comments"
//   | "blogEntry.view"
//   | "contest.hacks"
//   | "contest.list"
//   | "contest.ratingChanges"
//   | "contest.standings"
//   | "contest.status"
//   | "problemset.problems"
//   | "problemset.recentStatus"
//   | "recentActions"
//   | "user.blogEntries"
//   | "user.friends"
//   | "user.info"
//   | "user.ratedList"
//   | "user.rating"
//   | "user.status";

import { ProblemsetProblemsResult, Submission } from "./types";

export type Method =
  | "blogEntry.comments"
  | "problemset.problems"
  | "user.status";

export type MethodInteraction2 = {
  ["blogEntry.comments"]: {
    paramList: { blogEntryId: number };
    response: "ok";
  };
  ["problemset.problems"]: {
    paramList?: { tags?: string[]; problemsetName?: string };
    response: ProblemsetProblemsResult;
  };
  ["user.status"]: {
    paramList: {
      /**	Codeforces user handle. */
      handle: string;
      /** 1-based index of the first submission to return. */
      from?: number;
      /** Number of returned submissions. */
      count?: number;
    };
    response: Submission[];
  };
};

const apiEndpoint = "https://codeforces.com/api";

export const ApiMethodToUrl = (method: Method) => `${apiEndpoint}/${method}`;

export const AppendParamList = (
  subject: string,
  paramList?: { [key: string]: any },
) => {
  if (!paramList) return subject;
  const paramArray = Object.keys(paramList).map((e) => `${e}=${paramList[e]}`);
  if (paramArray.length === 0) return subject;
  const result = `${subject}?${paramArray.join("&")}`;
  console.log("after apend", result);
  return result;
};

export const fetchData = async <T extends Method>(
  method: T,
  paramList: MethodInteraction2[T]["paramList"],
) => {
  const fetching = fetch(
    AppendParamList(ApiMethodToUrl(method), paramList),
  ).then((e) => e.json());
  const res = await fetching;
  if (!res.result) return undefined;
  return res.result as MethodInteraction2[T]["response"];
};
