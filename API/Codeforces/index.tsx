import chroma from "chroma-js";
import { Centered } from "components/centered";
import { Fragment, useEffect, useState } from "react";
import {
  getContestIdList,
  getProblems,
  MergedData,
  MergedProblem,
} from "./API/advenced";
import { Submission } from "./API/types";

export type CfParserProps = any;

export const CfParser = (props: CfParserProps) => {
  const [problemList, setProblemList] = useState<MergedProblem[]>([]);
  const [submissions, setSubmissions] = useState<Submission[]>([]);
  const [mergedData, setMergedData] = useState<MergedData>({});
  const [pageElemCount, setPageElemCount] = useState(25);
  const getPageRange = (pageNumber: number) => {
    const start = pageNumber * pageElemCount;
    return { start, end: start + pageElemCount };
  };

  const [pageNum, setpageNum] = useState(0);
  const [pageRange, setpageRange] = useState(getPageRange(pageNum));
  useEffect(() => {
    setpageRange((_) => getPageRange(pageNum));
  }, [pageNum]);

  type ColorMapper = { [key: number]: chroma.Color };
  const [contestColor, setContestColor] = useState<ColorMapper>({
    [0]: chroma("white"),
  });

  useEffect(() => {
    const contestsIds = getContestIdList(problemList);
    const idxToColor = (n: number) => {
      return chroma.hsl((n * 140) % 360, 1, 0.5);
    };
    const result = contestsIds.reduce((prev, curr, idx) => {
      prev[curr] = idxToColor(idx);
      return prev;
    }, {} as ColorMapper);
    setContestColor(result);
  }, [problemList]);

  useEffect(() => {
    const getData = async () => {
      const result = await getProblems("tourist");
      setProblemList(result.problems);
      setSubmissions(result.submissionList);
      setMergedData(result.mergedData);
    };
    getData();
  }, []);

  return (
    <Fragment>
      <div style={{ position: "fixed", top: "50%" }}>
        {pageNum}
        <button
          onClick={() => {
            setpageNum((n) => Math.max(0, n - 1));
          }}
        >
          {"<-"}
        </button>
        <button
          onClick={() => {
            setpageNum((n) => Math.max(0, n + 1));
          }}
        >
          {"->"}
        </button>
      </div>
      <Centered style={{ flex: 1 }}>
        <table style={{ flex: 1 }}>
          <thead>
            <tr>
              <td>Code</td>
              <td>Problem Name</td>
              <td>Solved By</td>
            </tr>
          </thead>
          <tbody>
            {problemList
              ?.filter(
                (e, idx) => pageRange.start <= idx && idx < pageRange.end,
              )
              .map((e) => {
                const solved = mergedData[e.code].submissions?.some(
                  (e) => e.verdict === "OK",
                );
                const bg =
                  contestColor[e.contestId]
                    ?.brighten(0.5 + e.indexNumber * 0.2)
                    .hex() ?? "white";

                return (
                  <tr style={{ backgroundColor: bg }}>
                    <td>
                      {e.contestId}
                      {e.index}
                    </td>
                    <td>{e.name}</td>
                    <td
                      style={{
                        backgroundColor: solved ? "green" : "whitesmoke",
                      }}
                    >
                      {e.solvedCount}
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </Centered>
    </Fragment>
  );
};
