# CodeForces Viewer

This is a React Native application that displays [codeforces problems](https://codeforces.com/problemset) in a more convenient manner.
The app works by loading a webview and injecting styles and DOM manipulation JavaScript scripts.

<img src="doc/pics/problems_list.jpg" alt="problem_list" width="250"/>
<img src="doc/pics/problem_statment.jpg" alt="problem_list" width="250"/>
<img src="doc/pics/tutorial.jpg" alt="problem_list" width="250"/>

## Usage

- To view content in the problem tab, select a problem from the list of problems in the problem list tab.
- To view content in the tutorial tab, the problem statement tab must be loaded first.

## Installation

- install [expo](https://docs.expo.dev/get-started/installation/) then  
  `npx expo start`
- if you want use simulators: [android](https://docs.expo.dev/workflow/android-studio-emulator/) [ios](https://docs.expo.dev/workflow/ios-simulator/)

## Support

If codeforces changes the HTML DOM and the scripts are unable to recognize the new layout, or if there were no scripts initially set up to recognize it, the 'better' view will not be accessible until the scripts are modified or created.

## Roadmap

- [ ] Create DOM manipulating scripts for most of the cases of the codeforces HTML layout (problem statement HTML and editorial/tutorial HTML)
- [ ] Add a configuration option that allows the user to specify their username (currently hardcoded) and display preferences (such as font size and dark mode).
- [ ] Implement problem filters
- [ ] Build a separate API (as the codeforces API is not optimal)

## Contributing

If you have any changes or additions that you think would benefit this project, we encourage you to submit a pull request. All pull requests will be reviewed and considered for inclusion.

## Project status

I have had to suspend work on this project until further notice. It is currently unknown when I will be able to pick up where I left off.
